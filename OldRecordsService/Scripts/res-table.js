$(document).ready(function () {

    state = {
        isFly: false,
        isWidthСhange: false,
        isHeightСhange: false,
        isLastWidthСhange:false,
        $activeCol: null,
        $leftCol: null,
        $rightCol: null,
        startLeftColWidth: 0,
        startRightColWidth: 0,
        colCount: 0,
        startCursorX: 0,
        startCursorY: 0,
        currentCursorX: 0,
        currentCursorY: 0,
        startLeft: 0,
        startTop: 0,
        startRowHeight: 0,
        currentLeft: 0,
        currentTop: 0,
        colData: [],
        timeoutFlyID: 0,
        timeoutHide: [],
        timeoutShow: [],
        targetCol: 0,
        targetRow: 0,
        defaultTableWidth: 0,
        currentTableWidth: 0
    }

    options = {
        nameAttrCol: "res-col",
        nameAttrTargetCol: "target-col",
        nameAttrRow: "res-row",
        nameAttrTargetRow: "target-row",
        classFly: "fly",
        colMinWidth: 60,
        //urlSetTableProperty: "http://globalrecord.ru/Record/SetTableProperty",
        urlSetTableProperty: "http://localhost:54822/Record/SetTableProperty",
    }

    var isHeadControlDown = false;
    var headControl;

    function ConsoleError(errorStr) {        
        console.log("Responsive table: " + errorStr);
    }

    function getIsTableSuccess() {

        var isTableSuccess = true;

        var allHead = $(".col-res-table-head");
        var haedCount = allHead.length;
        
        var allCell = $(".res-table-body-cell");
        var cellCount = allCell.length;

        var allRow = $(".res-table-body-row");
        var rowCount = allRow.length;

        if (haedCount == 0) {
            isTableSuccess = false;
            ConsoleError("Table headers not found");
        }

        if (cellCount == 0) {
            isTableSuccess = false;
            ConsoleError("Table cells not found");
        }

        if (rowCount == 0) {
            isTableSuccess = false;
            ConsoleError("Table cells not found");
        }

        if ((cellCount / rowCount) != haedCount) {
            isTableSuccess = false;
            ConsoleError("The number of headers and columns does not match. Cannot build a table");
        }

        if (isTableSuccess) {
            state.colCount = haedCount;
        }

        return isTableSuccess;
    }

    function setStartCursor(e) {

        state.startCursorX = e.clientX;
        state.startCursorY = e.clientY;
    }

    function setCurrentCursor(e) {

        state.currentCursorX = e.clientX;
        state.currentCursorY = e.clientY;
    }

    function colFly() {

        clearTimeout(state.timeoutFlyID);
        state.isFly = true;
        state.$activeCol.addClass(options.classFly);
        state.$activeCol.css("z-index", 500);
    }

    function colNotFly() {

        state.isFly = false;
        state.$activeCol.removeClass(options.classFly);
        state.timeoutFlyID = setTimeout(function() {
            state.$activeCol.css("z-index", 0);
        }, 500);
    }

    function initTableHeader() {
        
        var allHead = $(".col-res-table-head");

        allHead.each(function(index, element) {

            var haed = $(element);
            var haedControl = haed.find(".head-control");

            haed.attr(options.nameAttrCol, index);
            haedControl.attr(options.nameAttrTargetCol, index);
        });
    }

    function initTableBody() {

        var allRow = $(".res-table-body-row");

        allRow.each(function(indexRow, elementRow) {

            var $row = $(elementRow);
            var rowCell = $row.find(".res-table-body-cell");
            var rowControls = $row.find(".cell-control-vertical");

            $row.attr(options.nameAttrRow, indexRow);
            rowControls.attr(options.nameAttrTargetRow, indexRow);

            rowCell.each(function(indexCell, elementCell) {

                var $cell = $(elementCell);
                $cell.attr(options.nameAttrCol, indexCell);
                $cell.find(".cell-control").attr(options.nameAttrTargetCol, indexCell);
            });

        });
    }

    function changeWidthToRight(leftColData, rightColData, deltaWidth) {

        leftColData.width = leftColData.width + Math.abs(deltaWidth);
        leftColData.rightSide = leftColData.rightSide + Math.abs(deltaWidth);
        leftColData.centerX = leftColData.leftSide + leftColData.width / 2;

        rightColData.width = rightColData.width - Math.abs(deltaWidth);
        rightColData.leftSide = rightColData.leftSide + Math.abs(deltaWidth);
        rightColData.centerX = rightColData.leftSide + rightColData.width / 2;

        state.$leftCol.outerWidth(leftColData.width);
        state.$rightCol.outerWidth(rightColData.width);
        state.$rightCol.css("left", rightColData.leftSide);
    }

    function changeWidthToLeft(leftColData, rightColData, deltaWidth) {

        leftColData.width = leftColData.width - Math.abs(deltaWidth);
        leftColData.rightSide = leftColData.rightSide - Math.abs(deltaWidth);
        leftColData.centerX = leftColData.leftSide + leftColData.width / 2;

        rightColData.width = rightColData.width + Math.abs(deltaWidth);
        rightColData.leftSide = rightColData.leftSide - Math.abs(deltaWidth);
        rightColData.centerX = rightColData.leftSide + rightColData.width / 2;

        state.$leftCol.outerWidth(leftColData.width);
        state.$rightCol.outerWidth(rightColData.width);
        state.$rightCol.css("left", rightColData.leftSide);
    }

    function setColWidth() {

        var deltaWidth = state.currentCursorX - state.startCursorX;
        var leftColData = state.colData[state.$leftCol.attr(options.nameAttrCol)];
        var rightColData = state.colData[state.$rightCol.attr(options.nameAttrCol)];

        if ((leftColData.width >= options.colMinWidth) && (rightColData.width >= options.colMinWidth)) {

            if (deltaWidth > 0) {
                //to right
                if (rightColData.width - Math.abs(deltaWidth) > options.colMinWidth) {
                    changeWidthToRight(leftColData, rightColData, deltaWidth);
                }

            } else if (deltaWidth < 0) {
                //to left
                if (leftColData.width - Math.abs(deltaWidth) > options.colMinWidth) {
                    changeWidthToLeft(leftColData, rightColData, deltaWidth);
                }
            }
        }
    }

    function setLastColWidthByTable() {

        var lastColData = getColDataByPosition(getLastVisiblePosition());
        var $lastCol = $("[" + options.nameAttrCol + " = " + lastColData.colID + "]");
        var $table = $(".res-table-container");

        var newColWidth = $table.width() - lastColData.leftSide;

        lastColData.width = newColWidth;
        lastColData.rightSide = $table.outerWidth();
        lastColData.centerX = lastColData.leftSide + lastColData.width / 2;
        
        $lastCol.outerWidth(lastColData.width);
        $lastCol.css("left", lastColData.leftSide);
    }

    function getSumColWidth() {

        var sumWidth = 0;

        for (var i = 0; i < state.colCount; i++) {
            if (state.colData[i].visible) {
                sumWidth = sumWidth + state.colData[i].width;
            }
        }

        return sumWidth;
    }

    function setLastColWidth() {

        var deltaWidth = state.currentCursorX - state.startCursorX;
        var rightColData = state.colData[state.$rightCol.attr(options.nameAttrCol)];

        if ((rightColData.width >= options.colMinWidth) && (getSumColWidth() > state.defaultTableWidth)) {

            if (deltaWidth < 0) {
                //to left
                if (rightColData.width - Math.abs(deltaWidth) > options.colMinWidth) {

                    rightColData.width = rightColData.width - Math.abs(deltaWidth);
                    rightColData.rightSide = rightColData.leftSide + rightColData.width;
                    rightColData.centerX = rightColData.leftSide + rightColData.width / 2;

                    state.$rightCol.outerWidth(rightColData.width);
                }    
            }
        }
    }
    
    function getColDataByPosition(position) {

        var col = {};

        for (var i = 0; i < state.colData.length; i++) {
            if (state.colData[i].position == position) {
                col = state.colData[i];
                break;
            }
        }

        return col;
    }

    function getColDataByID(colID) {

        var col = {};

        for (var i = 0; i < state.colData.length; i++) {
            if (state.colData[i].colID == colID) {
                col = state.colData[i];
                break;
            }
        }

        return col;
    }

    function swapColData(colLeftID, colRightID) {

        var buffer = {
            colID: state.colData[colLeftID].colID,
            position: state.colData[colLeftID].position,
            leftSide: state.colData[colLeftID].leftSide,
            rightSide: state.colData[colLeftID].rightSide,
            centerX: state.colData[colLeftID].centerX,
            width: state.colData[colLeftID].width
        };        

        //Set left col data
        state.colData[colLeftID].position = state.colData[colRightID].position;
        state.colData[colLeftID].leftSide = state.colData[colRightID].rightSide - state.colData[colLeftID].width;
        state.colData[colLeftID].rightSide = state.colData[colRightID].rightSide;
        state.colData[colLeftID].centerX = state.colData[colLeftID].leftSide + state.colData[colLeftID].width / 2;

        //Set right col data from buffer
        state.colData[colRightID].position = buffer.position;
        state.colData[colRightID].leftSide = buffer.leftSide;
        state.colData[colRightID].rightSide = state.colData[colRightID].leftSide + state.colData[colRightID].width;
        state.colData[colRightID].centerX = state.colData[colRightID].leftSide + state.colData[colRightID].width / 2;
    }

    function setColPosition(colID) {

        var colData = state.colData[colID];
        var $col = $("[" + options.nameAttrCol + " = " + colID + "]");

        $col.css("left", colData.leftSide);
    }

    function setColTopPosition(colID, top) {

        var $col = $("[" + options.nameAttrCol + " = " + colID + "]");

        $col.css("top", top);
    }

    function moveCol() {

        var deltaX = state.currentCursorX - state.startCursorX;
        var deltaY = state.currentCursorY - state.startCursorY;

        var left = state.startLeft + deltaX;
        state.currentTop = deltaY;
        state.$activeCol.css("left", left);

        var colID = state.targetCol;
        var col = state.colData[colID];

        setColTopPosition(colID, deltaY);

        if ((col.position > 0) && (col.position < state.colData.length - 1)) {

            var colLeft = getColDataByPosition(col.position - 1);
            var colRight = getColDataByPosition(col.position + 1);
            
            if (left < colLeft.centerX) {
                //Текущая колонка меняется с левой колонкой
                swapColData(colLeft.colID, col.colID);
                setColPosition(colLeft.colID);
            } else if (left + col.width > colRight.centerX) {
                //Текущая колонка меняется с правой колонкой
                swapColData(col.colID, colRight.colID);
                setColPosition(colRight.colID);
            }

        } else if (col.position == 0) {

            var colRight = getColDataByPosition(col.position + 1);

             if (left + col.width > colRight.centerX) {
                //Текущая колонка меняется с правой колонкой
                swapColData(col.colID, colRight.colID);
                setColPosition(colRight.colID);
            }

        } else if (col.position == state.colData.length - 1) {

            var colLeft = getColDataByPosition(col.position - 1);

            if (left < colLeft.centerX) {
                //Текущая колонка меняется с левой колонкой
                swapColData(colLeft.colID, col.colID);
                setColPosition(colLeft.colID);
            }

        }
    }

    function disabledLastCellControl() {

        var allCell = $(".cell-control");
        var lastColData = getColDataByPosition(getLastVisiblePosition());
        var lastCells = $("[" + options.nameAttrTargetCol + " = " + lastColData.colID + "].cell-control");

        allCell.removeClass("cell-control-disabled");
        lastCells.addClass("cell-control-disabled");
    }

    function setRowHeight() {

        var $row = $("[" + options.nameAttrRow + " = " + state.targetRow + "]");
        var deltaHeight = state.currentCursorY - state.startCursorY;

        if (deltaHeight > 0) {
            //To down
            $row.outerHeight(state.startRowHeight + Math.abs(deltaHeight));
            $row.find(".res-table-content").outerHeight(state.startRowHeight + Math.abs(deltaHeight) - 1);
        } else if (deltaHeight < 0) {
            //To up
            $row.outerHeight(state.startRowHeight - Math.abs(deltaHeight));
            $row.find(".res-table-content").outerHeight(state.startRowHeight - Math.abs(deltaHeight) - 1);
        }
    }

    function initMouseEvents() {

        $(document).mousedown(function (e) {

            var $target = $(e.target);
            setStartCursor(e);

            if ($target.hasClass("head-control")) {

                state.isFly = true;
                state.targetCol = $target.attr(options.nameAttrTargetCol);
                state.$activeCol = $("[" + options.nameAttrCol + " = " + state.targetCol + "]");
                colFly();
                state.startLeft = parseInt(state.$activeCol.css("left").replace("px", ""));
                state.startTop = parseInt(state.$activeCol.css("top").replace("px", ""));
            }

            if ($target.hasClass("cell-control") && !$target.hasClass("cell-control-disabled")) {

                state.isWidthСhange = true;
                state.targetCol = $target.attr(options.nameAttrTargetCol);
                
                var leftColData = getColDataByID(state.targetCol);
                state.$leftCol = $("[" + options.nameAttrCol + " = " + leftColData.colID + "]");

                var rightColData = getColDataByPosition(leftColData.position + 1);
                state.$rightCol = $("[" + options.nameAttrCol + " = " + rightColData.colID + "]");
               
                state.startLeftColWidth = state.$leftCol.outerWidth();
                state.startRightColWidth = state.$rightCol.outerWidth();
                state.$leftCol.addClass("change-width");
                state.$rightCol.addClass("change-width");
            }

            if ($target.hasClass("cell-control-disabled")) {

                state.isLastWidthСhange = true;

                var rightColData = getColDataByPosition(getLastVisiblePosition());
                state.$rightCol = $("[" + options.nameAttrCol + " = " + rightColData.colID + "]");

                state.startRightColWidth = state.$rightCol.outerWidth();
                state.$rightCol.addClass("change-width");
            }

            if ($target.hasClass("cell-control-vertical")) {

                state.isHeightСhange = true;
                state.targetRow = $target.attr(options.nameAttrTargetRow);
                var $row = $("[" + options.nameAttrRow + " = " + state.targetRow + "]");
                state.startRowHeight = $row.outerHeight();
            }

        });

        $(document).mousemove(function (e){
            
            e.preventDefault();

            if (state.isFly) { 
                setCurrentCursor(e);
                moveCol();
            }

            if (state.isWidthСhange) {
                setCurrentCursor(e);
                setColWidth();
                setStartCursor(e);
            }

            if (state.isLastWidthСhange) {
                setCurrentCursor(e);
                setLastColWidth();
                setStartCursor(e);
            }

            if (state.isHeightСhange) {
                setCurrentCursor(e);
                setRowHeight();
            }

        });

        $(document).mouseup(function (e){

            if (state.isFly) { 
                colNotFly();
                setColPosition(state.$activeCol.attr(options.nameAttrCol));
                state.isFly = false;
                setColTopPosition(state.targetCol, 0);
                disabledLastCellControl();

                //Submit changes to server
                SubmitTableProperty();
            }

            if (state.isWidthСhange) {
                state.isWidthСhange = false;
                state.$leftCol.removeClass("change-width");
                state.$rightCol.removeClass("change-width");

                //Submit changes to server
                SubmitTableProperty();
            }

            if (state.isLastWidthСhange) {
                state.isLastWidthСhange = false;
                state.$rightCol.removeClass("change-width");

                var $table = $(".res-table-container");
                var sumColWidth = getSumColWidth();

                //Если ширина таблицы получилась меньше минимальной из-за погрешности мыши, то делаем минимальную ширину таблице, и растягиваем последний столбец
                if (sumColWidth < state.defaultTableWidth) {

                    var deltaWidth = state.defaultTableWidth - sumColWidth;
                    var lastColData = getColDataByPosition(getLastVisiblePosition());
                    var newColWidth = lastColData.width + deltaWidth;
                    var $lastCol = $("[" + options.nameAttrCol + " = " + lastColData.colID + "]");

                    $table.width(state.defaultTableWidth);
                    
                    lastColData.width = newColWidth;
                    lastColData.rightSide = $table.outerWidth();
                    lastColData.centerX = lastColData.leftSide + lastColData.width / 2;

                    $lastCol.outerWidth(lastColData.width);
                    $lastCol.css("left", lastColData.leftSide);
                }

                //Submit changes to server
                SubmitTableProperty();
            }

            if (state.isHeightСhange) {
                state.isHeightСhange = false;
            }

        });
    }

    function getLastVisiblePositionWithoutDefaultWidth() {

        var position = getLastVisiblePosition() + 1;
        //Находим позицию самой правой видимой колонки, у которой ширина больше минимальной
        do {
            position = position - 1;
            var colData = getColDataByPosition(position);
            var width = colData.width;
        } while (width > options.defaultColWidth);

        return position;
    }

    function getLastVisiblePosition() {

        var position = -1;

        for (var i = 0; i < state.colData.length; i++) {
            
            if ((state.colData[i].visible) && (position < state.colData[i].position)) {
                position = state.colData[i].position;
            }
        }

        return position;
    }

    function SetColCssDisplay($col) {

        $col.each(function (index, element) {

            var $cell = $(element);

            if ($cell.hasClass("col-res-table-head")) {
                $cell.css("display", "flex");
            } else {
                $cell.css("display", "block");
            }
        });
    }

    function showCol(colID) {

        clearTimeout(state.timeoutHide[colID]);

        var colData = state.colData[colID];
        var leftBegine;
        var colWidth;
        var $col = $("[" + options.nameAttrCol + " = " + colID + "]");
        var $table = $(".res-table-container");

        if (getLastVisiblePosition() > -1) {
            //Если есть хотя бы одна отображаемая колонка, то ее правый край будет началом левого
            var lastColData = getColDataByPosition(getLastVisiblePosition());
            leftBegine = lastColData.rightSide;
            colWidth = colData.width;

            //Если появляющаяся колонка имеет позицию большую чем первой невидимой, то нужно поменять значения позиций
            if (colData.position > getLastVisiblePosition() + 1) {
                var firstHideColData = getColDataByPosition(getLastVisiblePosition() + 1);
                var bufferPosition = firstHideColData.position;

                firstHideColData.position = colData.position;
                colData.position = bufferPosition;
            }

            //Теперь появляющаяся колонка имеет позицию следующую за последнедней видимой
            colData.leftSide = leftBegine;
            colData.centerX = leftBegine + colWidth / 2;
            colData.rightSide = leftBegine + colWidth;
            colData.visible = true;

            $table.width($table.width() + colWidth);        
            $col.css("left", colData.leftSide);
            SetColCssDisplay($col);
            $col.outerWidth(colWidth);

        } else {
            //Если все колонки скрыты, то отображать начинаем с начала таблицы, а саму таблицу делаем шириной по умолчанию
            leftBegine = 0;
            colWidth = state.defaultTableWidth;
            $table.width(state.defaultTableWidth);

            getColDataByPosition(0).position = colData.position;
            colData.position = 0;

            colData.width = colWidth;
            colData.leftSide = leftBegine;
            colData.centerX = leftBegine + colWidth / 2;
            colData.rightSide = leftBegine + colWidth;
            colData.visible = true;

            $col.outerWidth(colWidth);
            $col.css("left", colData.leftSide);
            SetColCssDisplay($col);
        }

        state.timeoutHide[colID] = setTimeout(function() {
            $col.css("opacity", 1);
        }, 300);

        disabledLastCellControl();
        SubmitTableProperty();
    }

    function hideCol(colID) {

        clearTimeout(state.timeoutShow[colID]);

        var $col = $("[" + options.nameAttrCol + " = " + colID + "]");
        var colData = state.colData[colID];

        $col.css("opacity", 0);
        colData.visible = false;

        state.timeoutHide[colID] = setTimeout(function() {

            $col.css("display", "none");

            for (var position = colData.position; position < state.colData.length - 1; position++) {
                    
                var leftColData = getColDataByPosition(position);
                var rightColData = getColDataByPosition(position + 1);
                //Текущая колонка меняется с правой колонкой
                swapColData(leftColData.colID, rightColData.colID);
                setColPosition(rightColData.colID);
            }

            disabledLastCellControl();
            setLastColWidthByTable();
            SubmitTableProperty();

        }, 300);
    }

    function initPanelColHide() {

        var panelColHide = $(".res-table-panel-col-hide");

        for (var i = 0; i < state.colData.length; i++) {
            state.timeoutHide[i] = 0;
            state.timeoutShow[i] = 0;
        }
        
        for (var i = 0; i < state.colData.length; i++) {

            var title = $("[" + options.nameAttrCol + " = " + i + "].col-res-table-head").text();

            panelColHide.append(
                '<div class="res-table-panel-item">' +
                    '<input type="checkbox" checked="checked" id="res-table-checkbox-' + i + '" class="res-table-panel-checkbox" checkbox-target-col="' + i + '">' +
                    '<label for="res-table-checkbox-' + i + '" class="res-table-panel-label" >' + title + '</label>' +
                '</div>'
            );
        }

        var allChecbox = $(".res-table-panel-checkbox");

        allChecbox.each(function(index, element){

            var $checbox = $(element);

            //Установить обработчик клика
            $checbox.click(function () {
                if ($checbox.is(':checked')) {
                    showCol(index);
                } else {
                    hideCol(index);
                }   
            });   

            //Установить начальное значение
            if (!state.colData[index].visible) {
                $checbox.prop('checked', false);
            }

        });
    }

    function SubmitTableProperty() {

        var arrPosition = [];
        var arrVisible = [];
        var arrWidth = [];

        var strPosition = "";
        var strVisible = "";
        var strWidth = "";

        for (var i = 0; i < state.colCount; i++) {
            var colData = getColDataByID(i);
            arrPosition[i] = colData.position;
            arrVisible[i] = colData.visible;
            arrWidth[i] = Math.round(colData.width);
        }

        strPosition = arrPosition.join(",");
        strVisible = arrVisible.join(",");
        strWidth = arrWidth.join(",");

        var url = options.urlSetTableProperty + "?" + $.param({
            arrStrPosition: strPosition,
            arrStrVisible: strVisible,
            arrStrWidth: strWidth
        });

        $.ajax({
            url: url
        });
    }

    function setDefaultTableSize() {

        var defaultColWidth;
        var left = 0;
        var $table = $(".res-table-container");

        if ($table.width() > options.colMinWidth * state.colCount) {
            //Если ширина таблицы позволяет уместить все столбцы, то делим ширину равномерно по столбцам
            state.defaultTableWidth = $table.width();
            defaultColWidth = state.defaultTableWidth / state.colCount;
        } else {
            //Иначе делаем столбцам минимально разрешимую ширину и увеличиваем ширину таблицы
            state.defaultTableWidth = options.colMinWidth * state.colCount;
            defaultColWidth = options.colMinWidth;
            $table.width(state.defaultTableWidth);
        }

        state.currentTableWidth = state.defaultTableWidth;

        for (var i = 0; i < state.colCount; i++) {
            state.colData[i] = {
                colID: i,
                position: i,
                leftSide: left,
                rightSide: left + defaultColWidth,
                centerX: left + defaultColWidth / 2,
                width: defaultColWidth,
                visible: true
            };
            var col = $("[" + options.nameAttrCol + " = " + i + "]");
            col.outerWidth(defaultColWidth);
            col.css("left", left);
            left = left + defaultColWidth;
        }
    }

    function setTableSizeFromData(arrPositions, arrVisible, arrWidth) {

        var left = 0;
        var $table = $(".res-table-container");
        var sumWidth = 0;

        for (var i = 0; i < state.colCount; i++) {
            if (arrVisible[i]) {
                sumWidth = sumWidth + arrWidth[i];
            }
        }

        state.defaultTableWidth = $table.width();
        state.currentTableWidth = sumWidth;
        $table.width(sumWidth);

        //Поскольку колонки находятся в другом порядке, то для их отображения создаем массив, в котором индекс это номер позиции.
        var colsDataByPosition = [];
        for (var i = 0; i < state.colCount; i++) {

            var position = arrPositions[i];
            colsDataByPosition[position] = {
                colID: i,
                width: arrWidth[i],
                visible: arrVisible[i]
            }
        }

        // i - номер позиции
        for (var i = 0; i < state.colCount; i++) {

            var colData = colsDataByPosition[i];

            state.colData[colData.colID] = {
                colID: colData.colID,
                position: i,
                leftSide: left,
                rightSide: left + colData.width,
                centerX: left + colData.width / 2,
                width: colData.width,
                visible: colData.visible
            };

            var $col = $("[" + options.nameAttrCol + " = " + colData.colID + "]");

            if (!colData.visible) {
                $col.css("opacity", 0);
                $col.css("display", "none");
            }
            
            $col.outerWidth(colData.width);
            $col.css("left", left);
            left = left + colData.width;
        }
    }

    function initTableSize() {

        var arrPositions = $("#res-table-positions").text().split(",");
        var arrVisible = $("#res-table-visible").text().split(",");
        var arrWidth = $("#res-table-width").text().split(",");

        for (var i = 0; i < state.colCount; i++) {
            arrPositions[i] = parseInt(arrPositions[i]);
        }

        for (var i = 0; i < state.colCount; i++) {
            arrVisible[i] = arrVisible[i] === "True" ? true : false;
        }

        for (var i = 0; i < state.colCount; i++) {
            arrWidth[i] = parseInt(arrWidth[i]);
        }

        if (arrWidth[0] === 0) {
            setDefaultTableSize();
        } else {
            setTableSizeFromData(arrPositions, arrVisible, arrWidth);
        }
    }

    function initButtonDefault() {

        var $button = $("#res-table-action-default");
        
        $button.click(function () {

            //Показать все скрытые колонки            
            for (var i = 0; i < state.colCount; i++) {
                if (!state.colData[i].visible) {
                    var $col = $("[" + options.nameAttrCol + " = " + state.colData[i].colID + "]");
                    SetColCssDisplay($col);
                    $col.css("opacity", 1);
                }
            }

            //Установить все чекбоксы в cheked
            $(".res-table-panel-checkbox").prop('checked', true);

            //Сохранить настройки по умолчанию на сервере
            var arrPosition = [];
            var arrVisible = [];
            var arrWidth = [];

            var strPosition = "";
            var strVisible = "";
            var strWidth = "";

            for (var i = 0; i < state.colCount; i++) {
                arrPosition[i] = i;
                arrVisible[i] = true;
                arrWidth[i] = 0;
            }

            strPosition = arrPosition.join(",");
            strVisible = arrVisible.join(",");
            strWidth = arrWidth.join(",");

            var url = options.urlSetTableProperty + "?" + $.param({
                arrStrPosition: strPosition,
                arrStrVisible: strVisible,
                arrStrWidth: strWidth
            });

            $.ajax({
                url: url
            });

            //Устанавливаем ширину таблицы и столбцов по умолчанию
            var $table = $(".res-table-container");
            $table.width(state.defaultTableWidth);

            var defaultColWidth;
            var left = 0;

            if (state.defaultTableWidth > options.colMinWidth * state.colCount) {
                //Если ширина таблицы позволяет уместить все столбцы, то делим ширину равномерно по столбцам
                state.defaultTableWidth = state.defaultTableWidth;
                defaultColWidth = state.defaultTableWidth / state.colCount;
            } else {
                //Иначе делаем столбцам минимально разрешимую ширину и увеличиваем ширину таблицы
                state.defaultTableWidth = options.colMinWidth * state.colCount;
                defaultColWidth = options.colMinWidth;
                $table.width(state.defaultTableWidth);
            }

            state.currentTableWidth = state.defaultTableWidth;

            for (var i = 0; i < state.colCount; i++) {
                state.colData[i] = {
                    colID: i,
                    position: i,
                    leftSide: left,
                    rightSide: left + defaultColWidth,
                    centerX: left + defaultColWidth / 2,
                    width: defaultColWidth,
                    visible: true
                };
                var col = $("[" + options.nameAttrCol + " = " + i + "]");
                col.outerWidth(defaultColWidth);
                col.css("left", left);
                left = left + defaultColWidth;
            }

        });
    }

    function init() {

        if (getIsTableSuccess()) {
            initTableHeader();
            initTableBody();
            initTableSize();
            initMouseEvents();
            disabledLastCellControl();
            initPanelColHide();
            initButtonDefault();
        }
    }
        
    init();

});