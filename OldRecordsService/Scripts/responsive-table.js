$(document).ready(function () {

    //Responsive Table Row
    function initResponsiveTableRow () {

        var isRowSelect = false;
        var $row;
        var $rowText;
        var clientY;
        var currentClientY;
        var rowHeight;
        var rowTextHeight;
        var deltaHeight;
        var rowNumber;

        $(".responsive-table-row").mousedown( function(e) {

            rowNumber = $(e.currentTarget).attr("row-number");
            $row = $("[table-row-number = " + rowNumber + "]");
            $rowText = $row.find(".responsive-table-text");
            rowHeight = $row.height();
            rowTextHeight = $rowText.height();
            clientY = e.clientY;
            isRowSelect = true;

        });

        $(document).mousemove( function(e) {

            if (isRowSelect) {
                currentClientY = e.clientY;
                deltaHeight = currentClientY - clientY;
                $row.height(rowHeight + deltaHeight);
                $rowText.height(rowTextHeight + deltaHeight);
            }

        }); 

        $(document).mouseup( function() {
            isRowSelect = false;
        });        
    }

    //Responsive Table Col
    function initResponsiveTableCol () {

        var isColSelect = false;
        var $col;
        var clientX;
        var currentClientX;
        var deltaWidth;
        var colNumber;
        var colWidth;

        $(".responsive-table-col").mousedown( function(e) {

            colNumber = $(e.currentTarget).attr("col-number");
            $col = $("[table-col-number = " + colNumber + "]");
            colWidth = parseInt($col.attr("width"));
            clientX = e.clientX;
            isColSelect = true;

        });

        $(document).mousemove( function(e) {


            //Do not work correctly!!!
            if (isColSelect) {
                currentClientX = e.clientX;
                deltaWidth = currentClientX - clientX;
                var newWidth = colWidth + deltaWidth;
                if (newWidth > 0) {
                    $col.attr("width", newWidth);
                }
            }

        }); 

        $(document).mouseup( function() {
            isColSelect = false;
        });  
        
    }
    
    //Responsive Table Row
    initResponsiveTableRow();
    //Responsive Table Col
    //initResponsiveTableCol();

});