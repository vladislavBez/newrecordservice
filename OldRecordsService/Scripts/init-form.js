﻿$(document).ready(function () {

    var options = {
        //urlRecordList: "http://globalrecord.ru/Record/RecordList",
        //urlSingleRecord: "http://globalrecord.ru/Record/SingleRecord",
        //urlGetFile: "http://globalrecord.ru/Record/GetFile",
        //urlGetDocumentation: "http://globalrecord.ru/Record/GetDocumentation",
        urlRecordList: "http://localhost:54822/Record/RecordList",
        urlSingleRecord: "http://localhost:54822/Record/SingleRecord",
        urlGetFile: "http://localhost:54822/Record/GetFile",
        urlGetDocumentation: "http://localhost:54822/Record/GetDocumentation",
        formatExcel: "excel",
        formatWord: "word",
        formatPdf: "pdf"
    }

    function GetFile(format, isSingleRecord) {

        var selectVacancies = getStringValue($("#search-field-vacancy").select2('data'));
        var selectStatuses = getStringValue($("#search-field-status").select2('data'));
        var searchString = $("#search-field-common").val();
        var dateFrom = $("#search-field-date-from").val();
        var dateTo = $("#search-field-date-to").val();
        var showOldRecord = $("#search-field-old").is(':checked') ? true : false;
        var pageSize = $("#page-size-value").text();
        var sortOrder = $("#records-sort-order").text();
        var sortDirection = $("#records-sort-direction").text();
        var page = Number($("#page-value").text());

        var id = "";
        if (isSingleRecord == "True") {
            id = $("#single-record-id").text();
        }

        var url = options.urlGetFile + "?" + $.param({
            format: format,
            isSingleRecord: isSingleRecord,
            id: id,
            page: page,
            pageSize: pageSize,
            sortOrder: sortOrder,
            sortDirection: sortDirection,
            positions: null,
            statuses: selectStatuses,
            vacancies: selectVacancies,
            showOldRecord: showOldRecord,
            dateFrom: dateFrom,
            dateTo: dateTo,
            search: searchString
        });

        location.href = url;
    }

    function initHeaderActions() {

        var isSingleRecord = $("#is-single-record").text();

        $("#header-action-print").click(function () {
            window.print();
        });

        $("#header-action-excel").click(function () {
            GetFile(options.formatExcel, isSingleRecord);
        });

        $("#header-action-word").click(function () {
            GetFile(options.formatWord, isSingleRecord);
        });

        $("#header-action-pdf").click(function () {
            window.print();
        });

        $("#header-get-documentation").click(function () {
            var url = options.urlGetDocumentation;
            location.href = url;
        });
    }

    function initSelectInputs() {
        $("#search-field-position").select2({
            width: '100%'
        });
        $("#search-field-vacancy").select2({
            width: '100%'
        });
        $("#search-field-status").select2({
            width: '100%'
        });
        $("#select-count-row").select2({
            width: '90px'
        });
        $("#select-count-row").change(function () {
            getRecordList();
        });
    }

    function getValueArray(select2Array) {

        var valueArray = [];

        select2Array.forEach(function (item, i, arr) {
            valueArray[i] = item.text;
        });

        return valueArray;
    }

    function getStringValue(select2Array) {

        var stringValue = "";

        select2Array.forEach(function (item, i, arr) {
            stringValue = stringValue + item.text + ",";
        });

        stringValue = stringValue.substring(0, stringValue.length - 1);

        return stringValue;
    }

    function getStringValuePageSize(select2Array) {

        var stringValue = "";

        select2Array.forEach(function (item, i, arr) {
            var itemValue = item.text;
            if (itemValue == "Все") {
                itemValue = "999999"
            }
            stringValue = stringValue + itemValue + ",";
        });

        stringValue = stringValue.substring(0, stringValue.length - 1);

        return stringValue;
    }
    
    function getRecordList() {

        var selectVacancies = getStringValue($("#search-field-vacancy").select2('data'));
        var selectStatuses = getStringValue($("#search-field-status").select2('data'));
        var searchString = $("#search-field-common").val();
        var dateFrom = $("#search-field-date-from").val();
        var dateTo = $("#search-field-date-to").val();
        var showOldRecord = $("#search-field-old").is(':checked') ? true : false;
        var sortOrder = $("#records-sort-order").text();
        var sortDirection = $("#records-sort-direction").text();

        var pageSize;
        var isSingleRecord = $("#is-single-record").text();
        if (isSingleRecord == "True") {
            pageSize = $("#page-size-value").text();
        } else {
            pageSize = getStringValuePageSize($("#select-count-row").select2('data'));
        } 

        var url = options.urlRecordList + "?" + $.param({
            page: null,
            pageSize: pageSize,
            sortOrder: sortOrder,
            sortDirection: sortDirection,
            positions: null,
            statuses: selectStatuses,
            vacancies: selectVacancies,
            showOldRecord: showOldRecord,
            dateFrom: dateFrom,
            dateTo: dateTo,
            search: searchString
        });

        location.href = url;
    }

    function getSingleRecordUrl(id) {

        var selectVacancies = getStringValue($("#search-field-vacancy").select2('data'));
        var selectStatuses = getStringValue($("#search-field-status").select2('data'));
        var searchString = $("#search-field-common").val();
        var dateFrom = $("#search-field-date-from").val();
        var dateTo = $("#search-field-date-to").val();
        var showOldRecord = $("#search-field-old").is(':checked') ? true : false;
        var sortOrder = $("#records-sort-order").text();
        var sortDirection = $("#records-sort-direction").text();

        var page = Number($("#page-value").text());
        var pageSize = Number($("#page-size-value").text());

        var url = options.urlSingleRecord + "?" + $.param({
            id: id,
            page: page,
            pageSize: pageSize,
            sortOrder: sortOrder,
            sortDirection: sortDirection,
            positions: null,
            statuses: selectStatuses,
            vacancies: selectVacancies,
            showOldRecord: showOldRecord,
            dateFrom: dateFrom,
            dateTo: dateTo,
            search: searchString
        });

        return url;
    }

    function initSubmitForm() {

        var $buttonSubmit = $("#button-submit");

        $buttonSubmit.click(function () {
            getRecordList();            
        });
    }

    function initSingleRecordLink() {

        var allSingleRecordLink = $(".single-record-link");

        allSingleRecordLink.hover(function () {
            var activeLink = $(this);
            var id = activeLink.attr("id-record");
            activeLink.attr("href", getSingleRecordUrl(id));
        });

        allSingleRecordLink.contextmenu(function () {
            var activeLink = $(this);
            var id = activeLink.attr("id-record");
            activeLink.attr("href", getSingleRecordUrl(id));
        });
    }

    function initPanaginationLink() {

        var selectVacancies = getStringValue($("#search-field-vacancy").select2('data'));
        var selectStatuses = getStringValue($("#search-field-status").select2('data'));
        var searchString = $("#search-field-common").val();
        var dateFrom = $("#search-field-date-from").val();
        var dateTo = $("#search-field-date-to").val();
        var showOldRecord = $("#search-field-old").is(':checked') ? true : false;

        var $panaginationList = $(".panagination-list");
        var recordsCount = Number($("#records-count-value").text());
        var page = Number($("#page-value").text());
        var pageSize = Number($("#page-size-value").text());
        var sortOrder = $("#records-sort-order").text();
        var sortDirection = $("#records-sort-direction").text();

        var countLink = recordsCount / pageSize;

        for (pageNumber = 1; pageNumber < countLink; pageNumber++) {

            var url = options.urlRecordList + "?" + $.param({
                page: pageNumber,
                pageSize: pageSize,
                sortOrder: sortOrder,
                sortDirection: sortDirection,
                positions: null,
                statuses: selectStatuses,
                vacancies: selectVacancies,
                showOldRecord: showOldRecord,
                dateFrom: dateFrom,
                dateTo: dateTo,
                search: searchString
            });

            var classActive = "";
            if (pageNumber == page) {
                classActive = "active";
            }

            $panaginationList.append(
                '<li class="' + classActive + '"><a href="' + url + '" target-page="' + pageNumber + '" class="panagination-link">' + pageNumber + '</a></li>'
            );
        }
    }   

    function setSortTable(tableHeader) {

        var $tableHeader = $(tableHeader);
        var colNumber = $tableHeader.attr("res-col");
        var sortDirection;

        if ($tableHeader.hasClass("content-table-sort-asc")) {
            sortDirection = "desc";
        } else {
            sortDirection = "asc";
        }

        $("#records-sort-order").text(colNumber);
        $("#records-sort-direction").text(sortDirection);

        getRecordList();

    }

    function initTableSort() {

        var allTableHeaders = $(".content-table-head");
        var sortOrder = $("#records-sort-order").text();
        var sortDirection = $("#records-sort-direction").text(); 

        var $currentTableHeader;
        if (sortOrder != "") {
            $currentTableHeader = $(".content-table-head[res-col=" + sortOrder + "]");
            if (sortDirection == "desc") {
                $currentTableHeader.addClass("content-table-sort-desc");
            }
            if (sortDirection == "asc") {
                $currentTableHeader.addClass("content-table-sort-asc");
            }
        }

        allTableHeaders.each(function (index, element) {
            var $colHeader = $(element);
            $colHeader.click(function (e) {
                if (!$(e.target).hasClass("head-control")) {
                    setSortTable(this);
                }
            });
        });

    }

    initSelectInputs();
    initSubmitForm();
    initSingleRecordLink();
    initHeaderActions();
    initPanaginationLink();
    initTableSort();
});