﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(OldRecordsService.Startup))]
namespace OldRecordsService
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
