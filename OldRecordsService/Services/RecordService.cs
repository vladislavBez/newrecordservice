﻿using FileHelpers;
using FluentFTP;
using iTextSharp.text;
using iTextSharp.text.pdf;
using OldRecordService.Models.FileModels;
using OldRecordsService.BLL.ModelServises;
using OldRecordsService.Constants;
using OldRecordsService.Helpers;
using OldRecordsService.Models;
using OldRecordsService.Models.FileModels;
using OldRecordsService.Models.RecordModels;
using OldRecordsService.Models.ReportModels;
using PagedList;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web.Hosting;
using Xceed.Words.NET;

namespace OldRecordsService.Services
{
    public class RecordService
    {
        public RecordListViewModel GetResumeList(int? page, int? pageSize, string sortOrder, string sortDirection, string positions, string statuses, string vacancies, bool? showOldRecord, string dateFrom, string dateTo, string search, string userName)
        {
            RecordListViewModel recordList = new RecordListViewModel();

            int pageNumber = GlobalConstants.DEFAULT_PAGE_NUMBER;
            int pageCountRow = GlobalConstants.DEFAULT_ROW_COUNT;

            if (page != null)
            {
                pageNumber = (int)page;
            }

            if (pageSize != null)
            {
                pageCountRow = (int)pageSize;
            }

            if (positions == null)
            {
                positions = String.Empty;
            }

            if (statuses == null)
            {
                statuses = String.Empty;
            }

            if(vacancies == null)
            {
                vacancies = String.Empty;
            }

            if (showOldRecord == null)
            {
                showOldRecord = false;
            }

            if (dateFrom == null)
            {
                dateFrom = String.Empty;
            }

            if (dateTo == null)
            {
                dateTo = String.Empty;
            }

            if (search == null)
            {
                search = String.Empty;
            }

            List<RecordViewModel> records = new List<RecordViewModel>();
            RecordModelService recordModelService = new RecordModelService();
            records = recordModelService.GetResumeList(sortOrder, sortDirection, StringHelper.StringArrayToList(positions), StringHelper.StringArrayToList(statuses), StringHelper.StringArrayToList(vacancies), showOldRecord, DateHelper.GetDateFromString(dateFrom), DateHelper.GetDateFromString(dateTo), search, false, false);
            List<TablePropertyModel> tableProperties = recordModelService.GetTableProperties(userName);


            recordList.Records = records.ToPagedList(pageNumber, pageCountRow);
            recordList.Statuses = recordModelService.GetStatusList();
            recordList.Vacancies = recordModelService.GetVacancyList();
            recordList.CountRecords = records.Count;
            recordList.isSingleRecord = false;
            
            recordList.Filters = new FilterViewModel
            {
                DateFrom = dateFrom,
                DateTo = dateTo,
                Search = search,
                ShowOldRecords = (bool)showOldRecord,
                Statuses = statuses,
                Vacancies = vacancies,
                Page = pageNumber,
                PageSize = pageCountRow,
                SortOrder = sortOrder,
                SortDirection = sortDirection,
                PositionTableCols = FilterHelper.GetTableStringPositions(tableProperties),
                ShowTableCols = FilterHelper.GetTableStringVisible(tableProperties),
                WidthTableCols = FilterHelper.GetTableStringWidth(tableProperties),
                TableProperties = tableProperties
            };

            return recordList;
        }

        public RecordListViewModel GetResumeById(int? id, int? page, int? pageSize, string sortOrder, string sortDirection, string positions, string statuses, string vacancies, bool? showOldRecord, string dateFrom, string dateTo, string search, string userName)
        {
            RecordListViewModel recordList = new RecordListViewModel();
            RecordModelService recordModelService = new RecordModelService();

            int pageNumber = GlobalConstants.DEFAULT_PAGE_NUMBER;
            int pageCountRow = GlobalConstants.DEFAULT_ROW_COUNT;

            if (page != null)
            {
                pageNumber = (int)page;
            }

            if (pageSize != null)
            {
                pageCountRow = (int)pageSize;
            }

            if (positions == null)
            {
                positions = String.Empty;
            }

            if (statuses == null)
            {
                statuses = String.Empty;
            }

            if (vacancies == null)
            {
                vacancies = String.Empty;
            }

            if (showOldRecord == null)
            {
                showOldRecord = false;
            }

            if (dateFrom == null)
            {
                dateFrom = String.Empty;
            }

            if (dateTo == null)
            {
                dateTo = String.Empty;
            }

            if (search == null)
            {
                search = String.Empty;
            }

            int idRecord;
            if (id != null)
            {
                idRecord = (int)id;
                recordList.SingleRecord = recordModelService.GetResumeById(idRecord);
            }

            List<TablePropertyModel> tableProperties = recordModelService.GetTableProperties(userName);

            recordList.Statuses = recordModelService.GetStatusList();
            recordList.Vacancies = recordModelService.GetVacancyList();
            recordList.isSingleRecord = true;

            recordList.Filters = new FilterViewModel
            {
                DateFrom = dateFrom,
                DateTo = dateTo,
                Search = search,
                ShowOldRecords = (bool)showOldRecord,
                Statuses = statuses,
                Vacancies = vacancies,
                Page = pageNumber,
                PageSize = pageCountRow,
                SortOrder = sortOrder,
                SortDirection = sortDirection,
                PositionTableCols = FilterHelper.GetTableStringPositions(tableProperties),
                ShowTableCols = FilterHelper.GetTableStringVisible(tableProperties),
                WidthTableCols = FilterHelper.GetTableStringWidth(tableProperties),
                TableProperties = tableProperties
            };

            return recordList;
        }

        public void SetTableProperties(string userName, string arrStrPosition, string arrStrVisible, string arrStrWidth)
        {
            RecordModelService recordModelService = new RecordModelService();
            List<TablePropertyModel> tableProperties = new List<TablePropertyModel>();

            int[] arrPosition = arrStrPosition.Split(',').Select(p => int.Parse(p)).ToArray();
            bool[] arrVisible = arrStrVisible.Split(',').Select(v => bool.Parse(v)).ToArray();
            int[] arrWidth = arrStrWidth.Split(',').Select(w => int.Parse(w)).ToArray();

            for (int i = 0; i < GlobalConstants.DEFAULT_COL_COUNT; i++)
            {
                tableProperties.Add(new TablePropertyModel
                {
                    ColID = i,
                    UserName = userName,
                    Position = arrPosition[i],
                    Visible = arrVisible[i],
                    Width = arrWidth[i]
                });
            }

            recordModelService.SetTableProperties(tableProperties);
        }

        public void UploadCsvFiles(string dateStr)
        {
            FtpClient client = new FtpClient();

            //Create report
            ReportModel report = new ReportModel();

            string todayStr;
            if ((dateStr != null) || (DateHelper.GetDateFromString(dateStr) != null))
            {
                todayStr = dateStr;
            }
            else
            {
                //Create folder name (dotay in format dd-mm-yy 28-08-18)
                todayStr = DateHelper.GetTodayStringFormat();
            }

            try
            {
                //Create folder
                Directory.CreateDirectory(Path.Combine(HostingEnvironment.MapPath("~"), GlobalConstants.PATH_TO_UPLOADS_FOLDER, todayStr));
                report.IsCreateDirectory = true;
            }
            catch (Exception ex)
            {
                report.IsCreateDirectory = false;
                report.CreateDirectoryExceptionMessage = ex.Message;
                Console.WriteLine("IOException source: {0}", ex.Message);
            }
            
            //Create local folder name
            string uploadFolderName = Path.Combine(HostingEnvironment.MapPath("~"), GlobalConstants.PATH_TO_UPLOADS_FOLDER, todayStr);

            //Create local file names
            string localNameCvFile = Path.Combine(uploadFolderName, todayStr + GlobalConstants.FILE_NAME_CV + ".csv");
            string localNameCvEduFile = uploadFolderName + "\\" + todayStr + GlobalConstants.FILE_NAME_CV_EDU + ".csv";
            string localNameCvLangFile = uploadFolderName + "\\" + todayStr + GlobalConstants.FILE_NAME_CV_LANG + ".csv";
            string localNameCvProfFile = uploadFolderName + "\\" + todayStr + GlobalConstants.FILE_NAME_CV_PROF + ".csv";
            string localNameCvTestFile = uploadFolderName + "\\" + todayStr + GlobalConstants.FILE_NAME_CV_TEST + ".csv";

            //Create remote file names
            string remotePathCvFile = GlobalConstants.PATH_TO_REMOTE_DOWNLOAD_FOLDER + todayStr + "/" + todayStr + GlobalConstants.FILE_NAME_CV + ".csv";
            string remotePathCvEduFile = GlobalConstants.PATH_TO_REMOTE_DOWNLOAD_FOLDER + todayStr + "/" + todayStr + GlobalConstants.FILE_NAME_CV_EDU + ".csv";
            string remotePathCvLangFile = GlobalConstants.PATH_TO_REMOTE_DOWNLOAD_FOLDER + todayStr + "/" + todayStr + GlobalConstants.FILE_NAME_CV_LANG + ".csv";
            string remotePathCvProfFile = GlobalConstants.PATH_TO_REMOTE_DOWNLOAD_FOLDER + todayStr + "/" + todayStr + GlobalConstants.FILE_NAME_CV_PROF + ".csv";
            string remotePathCvTestFile = GlobalConstants.PATH_TO_REMOTE_DOWNLOAD_FOLDER + todayStr + "/" + todayStr + GlobalConstants.FILE_NAME_CV_TEST + ".csv";

            try
            {
                //Connect FTP
                client.Host = GlobalConstants.FTP_SERVER;
                client.Credentials = new NetworkCredential(GlobalConstants.FTP_LOGIN, GlobalConstants.FTP_PASSWORD);
                client.Port = GlobalConstants.FTP_PORT;

                client.Connect();

                report.IsConnectFTPSuccess = true;
            }
            catch (Exception ex)
            {
                report.IsConnectFTPSuccess = false;
                report.ConnectFTPExceptionMessage = ex.Message;
                Console.WriteLine("IOException source: {0}", ex.Message);
            }

            try
            {
                //Download files from server
                report.IsDownloadCvSuccess = client.DownloadFile(localNameCvFile, remotePathCvFile, true, FtpVerify.Retry);
                report.IsDownloadCvEduSuccess = client.DownloadFile(localNameCvEduFile, remotePathCvEduFile, true, FtpVerify.Retry);
                report.IsDownloadCvLangSuccess = client.DownloadFile(localNameCvLangFile, remotePathCvLangFile, true, FtpVerify.Retry);
                report.IsDownloadCvProfSuccess = client.DownloadFile(localNameCvProfFile, remotePathCvProfFile, true, FtpVerify.Retry);
                report.IsDownloadCvTestSuccess = client.DownloadFile(localNameCvTestFile, remotePathCvTestFile, true, FtpVerify.Retry);
            }
            catch (Exception ex)
            {
                report.DownloadCvExceptionMessage = ex.Message;
                Console.WriteLine("IOException source: {0}", ex.Message);
            }

            //Disonnect FTP
            client.Disconnect();

            if (report.IsDownloadCvSuccess &&
                report.IsDownloadCvEduSuccess &&
                report.IsDownloadCvLangSuccess &&
                report.IsDownloadCvProfSuccess &&
                report.IsDownloadCvTestSuccess)
            {
                try
                {
                    //Save file to DB
                    SaveCvToDB(todayStr);
                    SaveCvEduToDB(todayStr);
                    SaveCvLangToDB(todayStr);
                    SaveCvTestToDB(todayStr);
                    SaveCvProfToDB(todayStr);

                    report.IsSaveToDBSuccess = true;
                }
                catch (Exception ex)
                {
                    report.IsSaveToDBSuccess = false;
                    report.SaveToDBExceptionMessage = ex.Message;
                    Console.WriteLine("IOException source: {0}", ex.Message);
                }
            }

             CreateReportLogFile(report, todayStr);
        }

        public string CreateRecordListWord(RecordListViewModel recordList)
        {
            List<string> headers = FileHelper.GetTableHeaders();
            List<TablePropertyModel> tableProperties = recordList.Filters.TableProperties;
            DocX document = null;
            string path = Path.Combine(HostingEnvironment.MapPath("~"), GlobalConstants.PATH_TO_DOWNLOAD_FOLDER, GlobalConstants.NAME_RECORD_LIST_WORD);

            FileInfo file = new FileInfo(path);
            if (file.Exists)
            {
                file.Delete();
            }

            document = DocX.Create(path, DocumentTypes.Document);

            int countTableRow = recordList.Records.Count;
            int countTableCol = FilterHelper.GetVisibleCols(tableProperties).Count;
            Table table = document.AddTable(countTableRow + 1, countTableCol);

            //Create headers
            List<TablePropertyModel> visibleCols = FilterHelper.GetVisibleCols(tableProperties);

            for (int i = 0; i < visibleCols.Count; i++)
            {
                table.Rows[0].Cells[i].Paragraphs.First().Append(headers[FilterHelper.GetColByPosition(i, visibleCols).ColID]);
            }

            //Create all records
            for (int i = 1; i <= countTableRow; i++)
            {
                int numCol = 0;

                //Create coll num
                if (FilterHelper.GetIsColVisibleByID(0, tableProperties))
                {
                    table.Rows[i].Cells[FilterHelper.GetColPositionByID(0, tableProperties)].Paragraphs.First().Append(i.ToString());
                    numCol++;
                }
                //Create coll position
                if (FilterHelper.GetIsColVisibleByID(1, tableProperties))
                {
                    table.Rows[i].Cells[FilterHelper.GetColPositionByID(1, tableProperties)].Paragraphs.First().Append(recordList.Records[i - 1].Vacancy);
                    numCol++;
                }
                //Create coll name
                if (FilterHelper.GetIsColVisibleByID(2, tableProperties))
                {
                    table.Rows[i].Cells[FilterHelper.GetColPositionByID(2, tableProperties)].Paragraphs.First().Append(recordList.Records[i - 1].Title + recordList.Records[i - 1].FirstName + recordList.Records[i - 1].SurName);
                    numCol++;
                }
                //Create coll education
                if (FilterHelper.GetIsColVisibleByID(3, tableProperties))
                {
                    table.Rows[i].Cells[FilterHelper.GetColPositionByID(3, tableProperties)].Paragraphs.First().Append(recordList.Records[i - 1].EducationAvailable.Name);
                    numCol++;
                }
                //Create coll age
                if (FilterHelper.GetIsColVisibleByID(4, tableProperties))
                {
                    table.Rows[i].Cells[FilterHelper.GetColPositionByID(4, tableProperties)].Paragraphs.First().Append(recordList.Records[i - 1].Age.ToString());
                    numCol++;
                }
                //Create coll salary
                if (FilterHelper.GetIsColVisibleByID(5, tableProperties))
                {
                    table.Rows[i].Cells[FilterHelper.GetColPositionByID(5, tableProperties)].Paragraphs.First().Append(recordList.Records[i - 1].Salary);
                    numCol++;
                }
                //Create coll email
                if (FilterHelper.GetIsColVisibleByID(6, tableProperties))
                {
                    table.Rows[i].Cells[FilterHelper.GetColPositionByID(6, tableProperties)].Paragraphs.First().Append(recordList.Records[i - 1].Email);
                    numCol++;
                }
                //Create coll phone
                if (FilterHelper.GetIsColVisibleByID(7, tableProperties))
                {
                    table.Rows[i].Cells[FilterHelper.GetColPositionByID(7, tableProperties)].Paragraphs.First().Append(recordList.Records[i - 1].Phone);
                    numCol++;
                }
                //Create coll comment
                if (FilterHelper.GetIsColVisibleByID(8, tableProperties))
                {
                    table.Rows[i].Cells[FilterHelper.GetColPositionByID(8, tableProperties)].Paragraphs.First().Append(recordList.Records[i - 1].Comment);
                    numCol++;
                }
                //Create coll send date
                if (FilterHelper.GetIsColVisibleByID(9, tableProperties))
                {
                    table.Rows[i].Cells[FilterHelper.GetColPositionByID(9, tableProperties)].Paragraphs.First().Append(recordList.Records[i - 1].SendDate.Value.ToString("d"));
                    numCol++;
                }
                //Create coll komatsu comment
                if (FilterHelper.GetIsColVisibleByID(10, tableProperties))
                {
                    table.Rows[i].Cells[FilterHelper.GetColPositionByID(10, tableProperties)].Paragraphs.First().Append(recordList.Records[i - 1].KomatsuComment);
                    numCol++;
                }
                //Create coll army
                if (FilterHelper.GetIsColVisibleByID(11, tableProperties))
                {
                    table.Rows[i].Cells[FilterHelper.GetColPositionByID(11, tableProperties)].Paragraphs.First().Append(recordList.Records[i - 1].Army);
                    numCol++;
                }
                //Create coll driver
                if (FilterHelper.GetIsColVisibleByID(12, tableProperties))
                {
                    table.Rows[i].Cells[FilterHelper.GetColPositionByID(12, tableProperties)].Paragraphs.First().Append(recordList.Records[i - 1].Driver);
                    numCol++;
                }
                //Create coll status
                if (FilterHelper.GetIsColVisibleByID(13, tableProperties))
                {
                    table.Rows[i].Cells[FilterHelper.GetColPositionByID(13, tableProperties)].Paragraphs.First().Append(recordList.Records[i - 1].Status.Status);
                    numCol++;
                }
            }
            document.InsertTable(table);

            document.Save();

            return path;
        }

        public string CreateRecordListExcel(RecordListViewModel recordList)
        {
            List<string> headers = FileHelper.GetTableHeaders();
            List<string> abc = FileHelper.GetABC();
            List<TablePropertyModel> tableProperties = recordList.Filters.TableProperties;
            int countTableRow = recordList.Records.Count;
            string path = Path.Combine(HostingEnvironment.MapPath("~"), GlobalConstants.PATH_TO_DOWNLOAD_FOLDER, GlobalConstants.NAME_RECORD_LIST_EXCEL);
            FileInfo existFile = new FileInfo(path);
            if (existFile.Exists)
            {
                existFile.Delete();
            }

            ExcelHelper.CreateSpreadsheetWorkbook(path);

            //Create headers
            List<TablePropertyModel> visibleCols = FilterHelper.GetVisibleCols(tableProperties);

            for (int i = 0; i < visibleCols.Count; i++)
            {
                ExcelHelper.InsertText(path, abc[i], 1, headers[FilterHelper.GetColByPosition(i, visibleCols).ColID]);
            }

            //Create table content
            for (int i = 1; i <= countTableRow; i++)
            {
                int numCol = 0;
                //Create coll num
                if (FilterHelper.GetIsColVisibleByID(0, tableProperties))
                {
                    ExcelHelper.InsertText(path, abc[FilterHelper.GetColPositionByID(0, tableProperties)], (uint)(i + 1), i.ToString());
                    numCol++;
                }
                //Create coll position
                if (FilterHelper.GetIsColVisibleByID(1, tableProperties))
                {
                    ExcelHelper.InsertText(path, abc[FilterHelper.GetColPositionByID(1, tableProperties)], (uint)(i + 1), recordList.Records[i - 1].Vacancy);
                    numCol++;
                }
                //Create coll name
                if (FilterHelper.GetIsColVisibleByID(2, tableProperties))
                {
                    ExcelHelper.InsertText(path, abc[FilterHelper.GetColPositionByID(2, tableProperties)], (uint)(i + 1), recordList.Records[i - 1].Title + recordList.Records[i - 1].FirstName + recordList.Records[i - 1].SurName);
                    numCol++;
                }
                //Create coll education
                if (FilterHelper.GetIsColVisibleByID(3, tableProperties))
                {
                    ExcelHelper.InsertText(path, abc[FilterHelper.GetColPositionByID(3, tableProperties)], (uint)(i + 1), recordList.Records[i - 1].EducationAvailable.Name);
                    numCol++;
                }
                //Create coll age
                if (FilterHelper.GetIsColVisibleByID(4, tableProperties))
                {
                    ExcelHelper.InsertText(path, abc[FilterHelper.GetColPositionByID(4, tableProperties)], (uint)(i + 1), recordList.Records[i - 1].Age.ToString());
                    numCol++;
                }
                //Create coll salary
                if (FilterHelper.GetIsColVisibleByID(5, tableProperties))
                {
                    ExcelHelper.InsertText(path, abc[FilterHelper.GetColPositionByID(5, tableProperties)], (uint)(i + 1), recordList.Records[i - 1].Salary);
                    numCol++;
                }
                //Create coll email
                if (FilterHelper.GetIsColVisibleByID(6, tableProperties))
                {
                    ExcelHelper.InsertText(path, abc[FilterHelper.GetColPositionByID(6, tableProperties)], (uint)(i + 1), recordList.Records[i - 1].Email);
                    numCol++;
                }
                //Create coll phone
                if (FilterHelper.GetIsColVisibleByID(7, tableProperties))
                {
                    ExcelHelper.InsertText(path, abc[FilterHelper.GetColPositionByID(7, tableProperties)], (uint)(i + 1), recordList.Records[i - 1].Phone);
                    numCol++;
                }
                //Create coll comment
                if (FilterHelper.GetIsColVisibleByID(8, tableProperties))
                {
                    ExcelHelper.InsertText(path, abc[FilterHelper.GetColPositionByID(8, tableProperties)], (uint)(i + 1), recordList.Records[i - 1].Comment);
                    numCol++;
                }
                //Create coll send date
                if (FilterHelper.GetIsColVisibleByID(9, tableProperties))
                {
                    ExcelHelper.InsertText(path, abc[FilterHelper.GetColPositionByID(9, tableProperties)], (uint)(i + 1), recordList.Records[i - 1].SendDate.Value.ToString("d"));
                    numCol++;
                }
                //Create coll komatsu comment
                if (FilterHelper.GetIsColVisibleByID(10, tableProperties))
                {
                    ExcelHelper.InsertText(path, abc[FilterHelper.GetColPositionByID(10, tableProperties)], (uint)(i + 1), recordList.Records[i - 1].KomatsuComment);
                    numCol++;
                }
                //Create coll army
                if (FilterHelper.GetIsColVisibleByID(11, tableProperties))
                {
                    ExcelHelper.InsertText(path, abc[FilterHelper.GetColPositionByID(11, tableProperties)], (uint)(i + 1), recordList.Records[i - 1].Army);
                    numCol++;
                }
                //Create coll driver
                if (FilterHelper.GetIsColVisibleByID(12, tableProperties))
                {
                    ExcelHelper.InsertText(path, abc[FilterHelper.GetColPositionByID(12, tableProperties)], (uint)(i + 1), recordList.Records[i - 1].Driver);
                    numCol++;
                }
                //Create coll status
                if (FilterHelper.GetIsColVisibleByID(13, tableProperties))
                {
                    ExcelHelper.InsertText(path, abc[FilterHelper.GetColPositionByID(13, tableProperties)], (uint)(i + 1), recordList.Records[i - 1].Status.Status);
                    numCol++;
                }
            }
            
            return path;
        }

        public string CreateRecordListPdf(RecordListViewModel recordList)
        {
            List<string> headers = FileHelper.GetTableHeaders();
            Document document = new Document();
            string path = Path.Combine(HostingEnvironment.MapPath("~"), GlobalConstants.PATH_TO_DOWNLOAD_FOLDER, GlobalConstants.NAME_RECORD_LIST_PDF);
            List<TablePropertyModel> tableProperties = recordList.Filters.TableProperties;

            FileInfo file = new FileInfo(path);
            if (file.Exists)
            {
                file.Delete();
            }

            PdfWriter.GetInstance(document, new FileStream(path, FileMode.Create));
            document.Open();

            //Create table
            int countTableCol = FilterHelper.GetVisibleCols(tableProperties).Count;
            PdfPTable table = new PdfPTable(countTableCol);

            table.AddCell("Col 1 Row 1");
            table.AddCell("Col 2 Row 1");
            table.AddCell("Col 3 Row 1");
            table.AddCell("Col 1 Row 2");
            table.AddCell("Col 2 Row 2");
            table.AddCell("Col 3 Row 2");

            document.Add(table);
            document.Close();

            return path;
        }

        public string CreateRecordSingleWord(RecordListViewModel recordList)
        {
            List<string> headers = FileHelper.GetTableHeaders();
            DocX document = null;
            string path = Path.Combine(HostingEnvironment.MapPath("~"), GlobalConstants.PATH_TO_DOWNLOAD_FOLDER, GlobalConstants.NAME_RECORD_SINGLE_WORD);

            FileInfo file = new FileInfo(path);
            if (file.Exists)
            {
                file.Delete();
            }

            document = DocX.Create(path, DocumentTypes.Document);

            //Create styles
            string text = String.Empty;

            Formatting titleFormat = new Formatting
            {
                Size = 24D
            };

            Formatting headerFormat = new Formatting
            {
                Size = 20D
            };

            Formatting textFormat = new Formatting
            {
                Size = 16D
            };

            Formatting textBoldFormat = new Formatting();
            textFormat.Bold = true;

            //Create content of word document
            //Create title
            text = String.Format("{0} {1} {2}", recordList.SingleRecord.Title, recordList.SingleRecord.FirstName, recordList.SingleRecord.SurName);
            document.InsertParagraph(text, false, titleFormat);

            //Create common info
            text = String.Format("Основные данные");
            document.InsertParagraph(text, false, headerFormat);

            Table table = document.AddTable(8, 2);
            table.AutoFit = AutoFit.Window;
            table.Rows[0].Cells[0].Paragraphs.First().Append("Должность");
            table.Rows[0].Cells[1].Paragraphs.First().Append(recordList.SingleRecord.Vacancy);
            table.Rows[1].Cells[0].Paragraphs.First().Append("ID резюме");
            table.Rows[1].Cells[1].Paragraphs.First().Append(recordList.SingleRecord.Id.ToString());
            table.Rows[2].Cells[0].Paragraphs.First().Append("Отправлено");
            table.Rows[2].Cells[1].Paragraphs.First().Append(recordList.SingleRecord.SendDate.Value.ToString("d"));
            table.Rows[3].Cells[0].Paragraphs.First().Append("Опыт работы");
            table.Rows[3].Cells[1].Paragraphs.First().Append(recordList.SingleRecord.ExperienceAvailable.Period);
            table.Rows[4].Cells[0].Paragraphs.First().Append("Жалаемая зарплата");
            table.Rows[4].Cells[1].Paragraphs.First().Append(recordList.SingleRecord.Salary);
            table.Rows[5].Cells[0].Paragraphs.First().Append("Возраст");
            table.Rows[5].Cells[1].Paragraphs.First().Append(recordList.SingleRecord.Age.ToString());
            table.Rows[6].Cells[0].Paragraphs.First().Append("Email");
            table.Rows[6].Cells[1].Paragraphs.First().Append(recordList.SingleRecord.Email);
            table.Rows[7].Cells[0].Paragraphs.First().Append("Телефон");
            table.Rows[7].Cells[1].Paragraphs.First().Append(recordList.SingleRecord.Phone);

            document.InsertTable(table);

            //Create education info
            text = String.Format("Образование");
            document.InsertParagraph(text, false, headerFormat);

            Table tableEdu = document.AddTable(4, 2);
            tableEdu.AutoFit = AutoFit.Window;
            tableEdu.Rows[0].Cells[0].Paragraphs.First().Append("Образование");
            tableEdu.Rows[0].Cells[1].Paragraphs.First().Append(recordList.SingleRecord.EducationAvailable.Name);
            tableEdu.Rows[1].Cells[0].Paragraphs.First().Append("Учебные заведения");
            foreach (EducationViewModel edu in recordList.SingleRecord.Education)
            {
                tableEdu.Rows[1].Cells[1].Paragraphs.First().Append(edu.Name, textBoldFormat);
                tableEdu.Rows[1].Cells[1].Paragraphs.First().Append(edu.EndYear);
                tableEdu.Rows[1].Cells[1].Paragraphs.First().Append(edu.FacultyName);
            }
            tableEdu.Rows[2].Cells[0].Paragraphs.First().Append("Удостоверение по охране труда");
            tableEdu.Rows[2].Cells[1].Paragraphs.First().Append(recordList.SingleRecord.OccupationalSafetyHealth);
            tableEdu.Rows[3].Cells[0].Paragraphs.First().Append("Водительское удостоверение");
            tableEdu.Rows[3].Cells[1].Paragraphs.First().Append(recordList.SingleRecord.Driver);

            document.InsertTable(tableEdu);

            //Create experience info
            text = String.Format("Опыт работы");
            document.InsertParagraph(text, false, headerFormat);

            Table tableExp = document.AddTable(1, 2);
            tableExp.AutoFit = AutoFit.Window;
            tableExp.Rows[0].Cells[0].Paragraphs.First().Append("Предыдущие места работы");
            foreach (ExperienceViewModel exp in recordList.SingleRecord.Experience)
            {
                tableExp.Rows[0].Cells[1].Paragraphs.First().Append(exp.Organisation);
                if (exp.EndDate != null)
                {
                    tableExp.Rows[0].Cells[1].Paragraphs.First().Append("C " + exp.BeginDate.Value.ToString("d") + " по " + exp.EndDate.Value.ToString("d"));
                }
                else
                {
                    tableExp.Rows[0].Cells[1].Paragraphs.First().Append("C " + exp.BeginDate.Value.ToString("d") + " по настоящее время");
                }
                tableExp.Rows[0].Cells[1].Paragraphs.First().Append("Должность: " + exp.Position);
                tableExp.Rows[0].Cells[1].Paragraphs.First().Append("Обязанности: " + exp.Duty);
            }

            document.InsertTable(tableExp);

            //Create additional info
            text = String.Format("Дополнительно");
            document.InsertParagraph(text, false, headerFormat);

            Table tableInfo = document.AddTable(8, 2);
            tableInfo.AutoFit = AutoFit.Window;
            tableInfo.Rows[0].Cells[0].Paragraphs.First().Append("Комментарий");
            tableInfo.Rows[0].Cells[1].Paragraphs.First().Append(recordList.SingleRecord.Comment);
            tableInfo.Rows[1].Cells[0].Paragraphs.First().Append("Хобби, увлечения");
            tableInfo.Rows[1].Cells[1].Paragraphs.First().Append(recordList.SingleRecord.Hobby);
            tableInfo.Rows[2].Cells[0].Paragraphs.First().Append("Спорт");
            tableInfo.Rows[2].Cells[1].Paragraphs.First().Append(recordList.SingleRecord.Sport);
            tableInfo.Rows[3].Cells[0].Paragraphs.First().Append("Спортивные достижения");
            tableInfo.Rows[3].Cells[1].Paragraphs.First().Append(recordList.SingleRecord.SportProgress);
            tableInfo.Rows[4].Cells[0].Paragraphs.First().Append("Служба в армии");
            tableInfo.Rows[4].Cells[1].Paragraphs.First().Append(recordList.SingleRecord.Army);
            tableInfo.Rows[5].Cells[0].Paragraphs.First().Append("Комментарий KOMATSU");
            tableInfo.Rows[5].Cells[1].Paragraphs.First().Append(recordList.SingleRecord.KomatsuComment);
            tableInfo.Rows[6].Cells[0].Paragraphs.First().Append("Языки");
            foreach (LanguageViewModel lang in recordList.SingleRecord.Languages)
            {
                tableInfo.Rows[6].Cells[1].Paragraphs.First().Append(lang.Name);
                tableInfo.Rows[6].Cells[1].Paragraphs.First().Append(lang.Extent);
            }
            tableInfo.Rows[7].Cells[0].Paragraphs.First().Append("Курсы");
            foreach (TestViewModel test in recordList.SingleRecord.Tests)
            {
                tableInfo.Rows[7].Cells[1].Paragraphs.First().Append(test.Name);
                tableInfo.Rows[7].Cells[1].Paragraphs.First().Append(test.Organisation);
                tableInfo.Rows[7].Cells[1].Paragraphs.First().Append(test.Result);
            }

            document.InsertTable(tableInfo);

            document.Save();

            return path;
        }

        public string CreateRecordSingleExcel(RecordListViewModel recordList)
        {
            List<string> headers = FileHelper.GetTableHeaders();
            List<string> abc = FileHelper.GetABC();
            string text;
            string path = Path.Combine(HostingEnvironment.MapPath("~"), GlobalConstants.PATH_TO_DOWNLOAD_FOLDER, GlobalConstants.NAME_RECORD_SINGLE_EXCEL);
            FileInfo existFile = new FileInfo(path);
            if (existFile.Exists)
            {
                existFile.Delete();
            }

            ExcelHelper.CreateSpreadsheetWorkbook(path);

            //Create title
            text = String.Format("{0} {1} {2}", recordList.SingleRecord.Title, recordList.SingleRecord.FirstName, recordList.SingleRecord.SurName);
            ExcelHelper.InsertText(path, abc[0], 1, text);

            #region Create common info
            //Create common info
            text = "Основные данные";
            ExcelHelper.InsertText(path, abc[0], 2, text);

            text = "Должность";
            ExcelHelper.InsertText(path, abc[0], 3, text);

            text = recordList.SingleRecord.Vacancy;
            ExcelHelper.InsertText(path, abc[1], 3, text);

            text = "ID резюме";
            ExcelHelper.InsertText(path, abc[0], 4, text);

            text = recordList.SingleRecord.Id.ToString();
            ExcelHelper.InsertText(path, abc[1], 4, text);

            text = "Отправлено";
            ExcelHelper.InsertText(path, abc[0], 5, text);

            text = recordList.SingleRecord.SendDate.Value.ToString("d");
            ExcelHelper.InsertText(path, abc[1], 5, text);

            text = "Опыт работы";
            ExcelHelper.InsertText(path, abc[0], 6, text);

            text = recordList.SingleRecord.ExperienceAvailable.Period;
            ExcelHelper.InsertText(path, abc[1], 6, text);

            text = "Жалаемая зарплата";
            ExcelHelper.InsertText(path, abc[0], 7, text);

            text = recordList.SingleRecord.Salary;
            ExcelHelper.InsertText(path, abc[1], 7, text);
            
            text = "Возраст";
            ExcelHelper.InsertText(path, abc[0], 8, text);

            text = recordList.SingleRecord.Age.ToString();
            ExcelHelper.InsertText(path, abc[1], 8, text);

            text = "Email";
            ExcelHelper.InsertText(path, abc[0], 9, text);

            text = recordList.SingleRecord.Email;
            ExcelHelper.InsertText(path, abc[1], 9, text);

            text = "Телефон";
            ExcelHelper.InsertText(path, abc[0], 9, text);

            text = recordList.SingleRecord.Phone;
            ExcelHelper.InsertText(path, abc[1], 9, text);
            #endregion

            #region Create education info
            //Create education info
            text = "Образование";
            ExcelHelper.InsertText(path, abc[0], 10, text);

            text = "Образование";
            ExcelHelper.InsertText(path, abc[0], 11, text);

            text = recordList.SingleRecord.EducationAvailable.Name;
            ExcelHelper.InsertText(path, abc[1], 11, text);

            text = "Учебные заведения";
            ExcelHelper.InsertText(path, abc[0], 12, text);

            #endregion
            return path;
        }

        public string CreateRecordSinglePdf(RecordListViewModel recordList)
        {
            return "";
        }

        private void SaveCvToDB(string todayStr)
        {
            //Get new records
            string pathCvFile = Path.Combine(HostingEnvironment.MapPath("~"), GlobalConstants.PATH_TO_UPLOADS_FOLDER, todayStr, todayStr + GlobalConstants.FILE_NAME_CV + ".csv");
            FileHelperEngine<CvModel> engine = new FileHelperEngine<CvModel>(Encoding.UTF8);
            CvModel[] recordsNew = engine.ReadFile(pathCvFile);

            //Save all new entries
            RecordModelService recordModelService = new RecordModelService();
            foreach (var record in recordsNew)
            {
                if (!recordModelService.GetIsResumeExist(record.IdOld))
                {
                    recordModelService.SetResume(record);
                }
            }
        }

        private void SaveCvEduToDB(string todayStr)
        {
            //Get new records
            string pathCvEduFile = Path.Combine(HostingEnvironment.MapPath("~"), GlobalConstants.PATH_TO_UPLOADS_FOLDER, todayStr, todayStr + GlobalConstants.FILE_NAME_CV_EDU + ".csv");
            FileHelperEngine<CvEduModel> engine = new FileHelperEngine<CvEduModel>(Encoding.UTF8);
            CvEduModel[] recordsNew = engine.ReadFile(pathCvEduFile);

            //Save all new entries
            RecordModelService recordModelService = new RecordModelService();
            foreach (var record in recordsNew)
            {
                if (!recordModelService.GetIsEducationExist(record.IdOld))
                {
                    recordModelService.SetEducation(record);
                }
            }
        }

        private void SaveCvLangToDB(string todayStr)
        {
            //Get new records
            string pathCvLangFile = Path.Combine(HostingEnvironment.MapPath("~"), GlobalConstants.PATH_TO_UPLOADS_FOLDER, todayStr, todayStr + GlobalConstants.FILE_NAME_CV_LANG + ".csv");
            FileHelperEngine<CvLangModel> engine = new FileHelperEngine<CvLangModel>(Encoding.UTF8);
            CvLangModel[] recordsNew = engine.ReadFile(pathCvLangFile);

            //Save all new entries
            RecordModelService recordModelService = new RecordModelService();
            foreach (var record in recordsNew)
            {
                if (!recordModelService.GetIsLanguageExist(record.IdOld))
                {
                    recordModelService.SetLanguage(record);
                }
            }
        }

        private void SaveCvTestToDB(string todayStr)
        {
            //Get new records
            string pathCvTestFile = Path.Combine(HostingEnvironment.MapPath("~"), GlobalConstants.PATH_TO_UPLOADS_FOLDER, todayStr, todayStr + GlobalConstants.FILE_NAME_CV_TEST + ".csv");
            FileHelperEngine<CvTestModel> engine = new FileHelperEngine<CvTestModel>(Encoding.UTF8);
            CvTestModel[] recordsNew = engine.ReadFile(pathCvTestFile);

            //Save all new entries
            RecordModelService recordModelService = new RecordModelService();
            foreach (var record in recordsNew)
            {
                if (!recordModelService.GetIsTestExist(record.IdOld))
                {
                    recordModelService.SetTest(record);
                }
            }
        }

        private void SaveCvProfToDB(string todayStr)
        {
            //Get new records
             string pathCvProfFile = Path.Combine(HostingEnvironment.MapPath("~"), GlobalConstants.PATH_TO_UPLOADS_FOLDER, todayStr, todayStr + GlobalConstants.FILE_NAME_CV_PROF + ".csv");
            FileHelperEngine<CvProfModel> engine = new FileHelperEngine<CvProfModel>(Encoding.UTF8);
            CvProfModel[] recordsNew = engine.ReadFile(pathCvProfFile);

            //Save all new entries
            RecordModelService recordModelService = new RecordModelService();
            foreach (var record in recordsNew)
            {
                if (!recordModelService.GetIsExperienceExist(record.IdOld))
                {
                    recordModelService.SetExperience(record);
                }
            }
        }
        
        private void CreateReportLogFile(ReportModel report, string name)
        {
            string path = Path.Combine(HostingEnvironment.MapPath("~"), GlobalConstants.PATH_TO_LOGS_FOLDER, name);
            FileInfo file = new FileInfo(path);

            if (file.Exists)
            {
                File.Delete(path);
            }

            File.Create(path).Close();

            StreamWriter fileLog = new StreamWriter(path);

            fileLog.Write("IsCreateDirectory: ");
            fileLog.WriteLine(report.IsCreateDirectory ? "yes" : "no");
            fileLog.WriteLine("CreateDirectoryExceptionMessage: " + report.CreateDirectoryExceptionMessage);

            fileLog.Write("IsConnectFTPSuccess: ");
            fileLog.WriteLine(report.IsConnectFTPSuccess ? "yes" : "no");
            fileLog.WriteLine("ConnectFTPExceptionMessage: " + report.ConnectFTPExceptionMessage);

            fileLog.Write("IsDownloadCvSuccess: ");
            fileLog.WriteLine(report.IsDownloadCvSuccess ? "yes" : "no");
            fileLog.Write("IsDownloadCvEduSuccess: ");
            fileLog.WriteLine(report.IsDownloadCvEduSuccess ? "yes" : "no");
            fileLog.Write("IsDownloadCvLangSuccess: ");
            fileLog.WriteLine(report.IsDownloadCvLangSuccess ? "yes" : "no");
            fileLog.Write("IsDownloadCvProfSuccess: ");
            fileLog.WriteLine(report.IsDownloadCvProfSuccess ? "yes" : "no");
            fileLog.Write("IsDownloadCvTestSuccess: ");
            fileLog.WriteLine(report.IsDownloadCvTestSuccess ? "yes" : "no");
            fileLog.WriteLine("DownloadCvExceptionMessage: " + report.DownloadCvExceptionMessage);

            fileLog.Write("IsSaveToDBSuccess: ");
            fileLog.WriteLine(report.IsSaveToDBSuccess ? "yes" : "no");
            fileLog.WriteLine("SaveToDBExceptionMessage: " + report.SaveToDBExceptionMessage);

            fileLog.Close();
        }

    }
}