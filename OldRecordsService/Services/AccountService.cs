﻿using OldRecordsService.Constants;
using OldRecordsService.Helpers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Hosting;

namespace OldRecordsService.Services
{
    public class AccountService
    {
        public void WriteAccountLog(string status, string userName, DateTime date, string ip)
        {
            string path = Path.Combine(HostingEnvironment.MapPath("~"), GlobalConstants.PATH_TO_LOGS_FOLDER, GlobalConstants.NAME_ACCOUNT_LOG_FILE);
            string record = String.Format("{0} {1} {2} {3} {4}", date, status, userName, " IP: ", ip);

            if (!File.Exists(path))
            {
                File.Create(path).Close();
            }

            using (StreamWriter sw = new StreamWriter(path, true, System.Text.Encoding.Default))
            {
                sw.WriteLine(record);
                sw.Close();
            }
        }

        public bool GetIsIpValid(string ip)
        {
            List<string> allowedIps = AccountHelper.GetAllowedIps();
            bool res = false;

            if (allowedIps.Contains(ip))
            {
                res = true;
            }

            return res;
        }
    }
}