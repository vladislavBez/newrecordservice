﻿using ActionMailer.Net.Mvc;
using OldRecordsService.Models.EmailModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OldRecordsService.Services
{
    public class MailService : MailerBase
    {
        public EmailResult SendEmail(ReportEmailModel model)
        {
            To.Add(model.To);

            From = model.From;

            Subject = model.Subject;

            return Email("ReportMail", model);
        }
    }
}