﻿using FileHelpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OldRecordsService.Models.FileModels
{
    [DelimitedRecord("`")]
    [IgnoreEmptyLines]
    public class CvProfModel
    {
        public int IdOld { get; set; }
        public int CvId { get; set; }
        //Date format "yyyy-mm-dd"
        public string BeginDate { get; set; }
        //Date format "yyyy-mm-dd"
        public string EndDate { get; set; }
        public string Organisation { get; set; }
        public string Position { get; set; }
        public string Duty { get; set; }
    }
}