﻿using FileHelpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OldRecordService.Models.FileModels
{
    [DelimitedRecord("#")]
    [IgnoreEmptyLines]
    public class CvModel
    {
        public int IdOld { get; set; }
        public int StatusId { get; set; }
        public string FirstName { get; set; }
        public string Title { get; set; }
        public string SurName { get; set; }
        public int Age { get; set; }
        //Date format "yyyy-mm-dd"
        public string Birthday { get; set; }
        public int EducationId { get; set; }
        public string Vacancy { get; set; }
        public int ExperienceId { get; set; }
        public string Salary { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string Comment { get; set; }
        public string CvSession { get; set; }
        public bool IsSend { get; set; }
        //Date format "yyyy-mm-dd"
        public string SendDate { get; set; }
        public string Hobby { get; set; }
        public string Sport { get; set; }
        public string SportProgress { get; set; }
        public string KomatsuComment { get; set; }
        public bool IsPublish { get; set; }
        public string Army { get; set; }
        public string Driver { get; set; }
        public string OccupationalSafetyHealth { get; set; }
        public bool IsViewed { get; set; }
        public bool IsAgreePersonalData { get; set; }
    }
}