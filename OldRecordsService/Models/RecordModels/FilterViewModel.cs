﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OldRecordsService.Models.RecordModels
{
    public class FilterViewModel
    {
        public string Statuses { get; set; }
        public string Vacancies { get; set; }
        public string Search { get; set; }
        public string DateFrom { get; set; }
        public string DateTo { get; set; }
        public bool ShowOldRecords { get; set; }
        public int Page { get; set; }
        public int PageSize { get; set; }
        public string SortOrder { get; set; }
        public string SortDirection { get; set; }
        public List<TablePropertyModel> TableProperties { get; set; }
        public string ShowTableCols { get; set; }
        public string PositionTableCols { get; set; }
        public string WidthTableCols { get; set; }
    }
}