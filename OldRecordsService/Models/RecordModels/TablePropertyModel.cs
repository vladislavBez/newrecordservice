﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OldRecordsService.Models.RecordModels
{
    public class TablePropertyModel
    {
        public int Id { get; set; }
        public string UserName { get; set; }
        public int ColID { get; set; }
        public int Position { get; set; }
        public int Width { get; set; }
        public bool Visible { get; set; }
    }
}