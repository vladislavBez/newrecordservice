﻿using OldRecordsService.Models.ReportModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OldRecordsService.Models.EmailModels
{
    public class ReportEmailModel
    {
        public string Subject { get; set; }

        public string From { get; set; }

        public string To { get; set; }

        public ReportModel Body { get; set; }
    }
}