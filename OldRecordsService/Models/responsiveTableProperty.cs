//------------------------------------------------------------------------------
// <auto-generated>
//     Этот код создан по шаблону.
//
//     Изменения, вносимые в этот файл вручную, могут привести к непредвиденной работе приложения.
//     Изменения, вносимые в этот файл вручную, будут перезаписаны при повторном создании кода.
// </auto-generated>
//------------------------------------------------------------------------------

namespace OldRecordsService.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class responsiveTableProperty
    {
        public int id { get; set; }
        public string userName { get; set; }
        public int colID { get; set; }
        public int position { get; set; }
        public bool visible { get; set; }
        public int width { get; set; }
    }
}
