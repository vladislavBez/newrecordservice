﻿using OldRecordsService.Constants;
using OldRecordsService.Helpers;
using OldRecordsService.Models.RecordModels;
using OldRecordsService.Services;
using System;
using System.IO;
using System.Web.Hosting;
using System.Web.Mvc;
using static System.Net.Mime.MediaTypeNames;

namespace OldRecordsService.Controllers
{
    public class RecordController : Controller
    {
        // GET: Record/
        public ActionResult Index()
        {
            AccountService accountService = new AccountService();
            string userName = User.Identity.Name;
            DateTime today = DateTime.Now;
            string ip = Request.UserHostAddress;
            accountService.WriteAccountLog(GlobalConstants.ACCOUNT_LOG_LOGIN, userName, today, ip);

            return RedirectToAction("RecordList", "Record");
        }

        // GET: Record/RecordList
        [Authorize]
        public ActionResult RecordList(int? page, int? pageSize, string sortOrder, string sortDirection, string positions, string statuses, string vacancies, bool? showOldRecord, string dateFrom, string dateTo, string search)
        {
            RecordService recordService = new RecordService();
            string userName = User.Identity.Name;
            RecordListViewModel records = recordService.GetResumeList(page, pageSize, sortOrder, sortDirection, positions, statuses, vacancies, showOldRecord, dateFrom, dateTo, search, userName);
            records.UserLogin = User.Identity.Name;

            return View("RecordList", records);
        }

        // GET: Record/SingleRecord
        [Authorize]
        public ActionResult SingleRecord(int? id, int? page, int? pageSize, string sortOrder, string sortDirection, string positions, string statuses, string vacancies, bool? showOldRecord, string dateFrom, string dateTo, string search)
        {
            ViewResult viewResult = new ViewResult();
            RecordService recordService = new RecordService();
            string userName = User.Identity.Name;

            if (id != null)
            {
                RecordListViewModel record = recordService.GetResumeById(id, page, pageSize, sortOrder, sortDirection, positions, statuses, vacancies, showOldRecord, dateFrom, dateTo, search, userName);
                record.UserLogin = User.Identity.Name;
                viewResult = View("SingleRecord", record);
            }
            else
            {
                viewResult = View("NotFoundRecord");
            }           

            return viewResult;
        }
        
        // GET: Record/UpdateRecords/28-08-18
        public string UpdateRecords(string dateStr)
        {
            RecordService recordService = new RecordService();
            recordService.UploadCsvFiles(dateStr);
            return "";
        }

        // GET: Record/GetFile
        [Authorize]
        public FileResult GetFile(string format, bool isSingleRecord, int? id, int? page, int? pageSize, string sortOrder, string sortDirection, string positions, string statuses, string vacancies, bool? showOldRecord, string dateFrom, string dateTo, string search)
        {
            RecordService recordService = new RecordService();
            RecordListViewModel records;
            string fileName = String.Empty;
            string userName = User.Identity.Name;

            //Get records data
            if (isSingleRecord)
            {
                records = recordService.GetResumeById(id, page, pageSize, sortOrder, sortDirection, positions, statuses, vacancies, showOldRecord, dateFrom, dateTo, search, userName);
            }
            else
            {
                records = recordService.GetResumeList(page, pageSize, sortOrder, sortDirection, positions, statuses, vacancies, showOldRecord, dateFrom, dateTo, search, userName);
            }

            //Get file by format and type of page
            string path = String.Empty;
            if (isSingleRecord)
            {
                //select format of single record
                switch (format)
                {
                    case GlobalConstants.FORMAT_EXCEL:
                        path = recordService.CreateRecordSingleExcel(records);
                        fileName = FileHelper.GetSingleFileName(records.SingleRecord, GlobalConstants.EXTENSION_EXCEL);
                        break;
                    case GlobalConstants.FORMAT_WORD:
                        path = recordService.CreateRecordSingleWord(records);
                        fileName = FileHelper.GetSingleFileName(records.SingleRecord, GlobalConstants.EXTENSION_WORD);
                        break;
                    case GlobalConstants.FORMAT_PDF:
                        path = recordService.CreateRecordSinglePdf(records);
                        fileName = GlobalConstants.NAME_RECORD_SINGLE_PDF;
                        break;
                }
            }
            else
            {
                //select format of record list
                switch (format)
                {
                    case GlobalConstants.FORMAT_EXCEL:
                        path = recordService.CreateRecordListExcel(records);
                        fileName = GlobalConstants.NAME_RECORD_LIST_EXCEL;
                        break;
                    case GlobalConstants.FORMAT_WORD:
                        path = recordService.CreateRecordListWord(records);
                        fileName = GlobalConstants.NAME_RECORD_LIST_WORD;
                        break;
                    case GlobalConstants.FORMAT_PDF:
                        path = recordService.CreateRecordListPdf(records);
                        fileName = GlobalConstants.NAME_RECORD_LIST_PDF;
                        break;
                }
            }

            byte[] fileBytes = System.IO.File.ReadAllBytes(path);
            
            return File(fileBytes, Application.Octet, fileName);
        }

        // GET: Record/Documentation
        [Authorize]
        public FileResult GetDocumentation()
        {
            string path = Path.Combine(HostingEnvironment.MapPath("~"), GlobalConstants.PATH_TO_DOWNLOAD_FOLDER, GlobalConstants.NAME_DOCUMENTATION_WORD);

            byte[] fileBytes = System.IO.File.ReadAllBytes(path);

            return File(fileBytes, Application.Octet, GlobalConstants.NAME_DOCUMENTATION_WORD);
        }

        // GET: Record/SetTableProperty
        [Authorize]
        public void SetTableProperty(string arrStrPosition, string arrStrVisible, string arrStrWidth)
        {
            string userName = User.Identity.Name;

            RecordService recordService = new RecordService();
            recordService.SetTableProperties(userName, arrStrPosition, arrStrVisible, arrStrWidth);
        }

    }
}