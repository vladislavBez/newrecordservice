﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OldRecordsService.Constants
{
    public static class GlobalConstants
    {
        public const string PATH_TO_UPLOADS_FOLDER = "App_Data\\Uploads\\";
        public const string PATH_TO_LOGS_FOLDER = "App_Data\\Logs\\";
        public const string PATH_TO_DOWNLOAD_FOLDER = "App_Data\\Downloads\\";
        public const string PATH_TO_REMOTE_DOWNLOAD_FOLDER = "/www/Records/";
        public const string FILE_NAME_CV = "-cv";
        public const string FILE_NAME_CV_EDU = "-cv-edu";
        public const string FILE_NAME_CV_LANG = "-cv-lang";
        public const string FILE_NAME_CV_PROF = "-cv-prof";
        public const string FILE_NAME_CV_TEST = "-cv-test";

        public const string FTP_SERVER = "213.187.99.145";
        public const int FTP_PORT = 21;
        public const string FTP_LOGIN = "komatsu";
        public const string FTP_PASSWORD = "ftve818h";

        public const int DEFAULT_PAGE_NUMBER = 1;
        public const int DEFAULT_ROW_COUNT = 20;
        public const int DEFAULT_COL_COUNT = 15;

        public const string NAME_DOCUMENTATION_WORD = "Документация к архиву резюме.docx";
        public const string NAME_ACCOUNT_LOG_FILE = "account-log";
        public const string ACCOUNT_LOG_LOGIN = "Login to account:";
        public const string ACCOUNT_LOG_EXIT = "Logout:";
        public const string ACCOUNT_LOG_UNAUTHORIZED_IP = "Login from unauthorized IP:";

        public const string NAME_RECORD_LIST_WORD = "record-list.docx";
        public const string NAME_RECORD_LIST_EXCEL = "record-list.xlsx";
        public const string NAME_RECORD_LIST_PDF = "record-list.pdf";

        public const string NAME_RECORD_SINGLE_WORD = "record-single.docx";
        public const string NAME_RECORD_SINGLE_EXCEL = "record-single.xlsx";
        public const string NAME_RECORD_SINGLE_PDF = "record-single.pdf";

        public const string EXTENSION_WORD = ".docx";
        public const string EXTENSION_EXCEL = ".xlsx";

        public const string FORMAT_WORD = "word";
        public const string FORMAT_EXCEL = "excel";
        public const string FORMAT_PDF = "pdf";

        //public const string ROOT_URL = "http://localhost:54449";
        public const string ROOT_URL = "http://globalrecord.ru/PublishOutput";

    }
}