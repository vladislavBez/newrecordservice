﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OldRecordsService.Helpers
{
    public static class AccountHelper
    {
        public static List<string> GetAllowedIps()
        {
            string[] ips = { "::1", "176.214.82.148", "188.128.21.222", "213.150.81.182", "193.232.111.0" };

            return new List<string>(ips);
        }
    }
}