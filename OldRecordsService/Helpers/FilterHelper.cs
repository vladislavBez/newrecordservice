﻿using OldRecordsService.Constants;
using OldRecordsService.Models.RecordModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OldRecordsService.Helpers
{
    public static class FilterHelper
    {
        public static string GetTableStringPositions(List<TablePropertyModel> properties)
        {
            string res = "";

            if (properties.Count > 0)
            {
                string delimiter = ",";

                foreach (var property in properties)
                {
                    res = res + property.Position + delimiter;
                }
                res = res.Substring(0, res.Length - 1);
            }

            return res;
        }

        public static string GetTableStringWidth(List<TablePropertyModel> properties)
        {
            string res = "";

            if (properties.Count > 0)
            {
                string delimiter = ",";

                foreach (var property in properties)
                {
                    res = res + property.Width + delimiter;
                }
                res = res.Substring(0, res.Length - 1);
            }

            return res;
        }

        public static string GetTableStringVisible(List<TablePropertyModel> properties)
        {
            string res = "";

            if (properties.Count > 0)
            {
                string delimiter = ",";

                foreach (var property in properties)
                {
                    res = res + property.Visible + delimiter;
                }
                res = res.Substring(0, res.Length - 1);
            }

            return res;
        }

        public static int GetColPositionByID(int id, List<TablePropertyModel> properties)
        {
            int res = properties.FirstOrDefault(p => p.ColID == id).Position;

            return res;
        }

        public static bool GetIsColVisibleByID(int id, List<TablePropertyModel> properties)
        {
            bool res = properties.FirstOrDefault(p => p.ColID == id).Visible;

            return res;
        }

        public static TablePropertyModel GetColByPosition(int position, List<TablePropertyModel> properties)
        {
            TablePropertyModel res = properties.FirstOrDefault(p => p.Position == position);

            return res;
        }

        public static TablePropertyModel GetColByID(int id, List<TablePropertyModel> properties)
        {
            TablePropertyModel res = properties.FirstOrDefault(p => p.ColID == id);

            return res;
        }

        public static List<TablePropertyModel> GetVisibleCols(List<TablePropertyModel> properties)
        {
            List<TablePropertyModel> res = properties.Where(p => p.Visible).ToList();

            return res;
        }

        public static List<TablePropertyModel> GetDefaultColData()
        {
            List<TablePropertyModel> properties = new List<TablePropertyModel>();

            for (int i = 0; i < GlobalConstants.DEFAULT_COL_COUNT; i++)
            {
                properties.Add(new TablePropertyModel
                {
                    Position = i,
                    ColID = i,
                    Visible = true,
                });
            }

            return properties;
        }
    }
}