﻿using OldRecordsService.Constants;
using OldRecordsService.Models.RecordModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OldRecordsService.Helpers
{
    public static class FileHelper
    {
        public static List<string> GetTableHeaders()
        {
            string[] headers = { "№", "Должность", "ФИО", "Образование", "Возраст", "Зарплата", "Email", "Телефон", "Комментарий", "Дата отправки", "Комментарий от Komatsu", "Армия", "Водитель", "Статус", "" };

            return new List<string>(headers);
        }

        public static List<string> GetABC()
        {
            string[] abc = { "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z" };

            return new List<string>(abc);
        }

        public static string GetSingleFileName(RecordViewModel record, string extension)
        {
            string name = String.Format("{0} {1} {2}{3}", record.Title, record.FirstName, record.SurName, extension);

            return name;
        }

    }
}